package msgServer;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by Liam on 15/03/2017.
 */

// This class allows a new user to be registered into the server and be added within the database.
    //EG:
    //107 (Command Number)
    //Enter Username (bob)
    //Enter Password (password123)
    //Enter DOB (02/06/1990)
    //Enter Address (258 test lane)
    //Enter Telephone Number (0123654785)
public class RegisterUser implements Command
{
    private BufferedReader in;
    private BufferedWriter out;
    private MsgSvrConnection conn;

    public RegisterUser(BufferedReader in, BufferedWriter out, MsgSvrConnection serverConn)
    {
        this.in = in;
        this.out = out;
        this.conn = serverConn;
    }

    public void execute() throws IOException
    {
        conn.getServer().sendDebugPromts(out, "Username:");
        String username = in.readLine();

        conn.getServer().sendDebugPromts(out, "Password:");
        String password = in.readLine();

        conn.getServer().sendDebugPromts(out, "DOB (dd/mm/yyyy):");
        String dob = in.readLine();

            try {
                SimpleDateFormat sqlFormatter = new SimpleDateFormat("yyyy-MM-dd");
                SimpleDateFormat inputFormatter = new SimpleDateFormat("dd/MM/yyyy");
                Date date = inputFormatter.parse(dob);
                dob = sqlFormatter.format(date);
            } catch (Exception e) {
                new ErrorCommand(in, out, conn, "Date formatted wrong").execute();
                return;
            }

        conn.getServer().sendDebugPromts(out, "Address:");
        String address = in.readLine();

        conn.getServer().sendDebugPromts(out, "Telephone:");
        String telephone = in.readLine();

        Database db = conn.getServer().db;
        try
        {
            db.registerUser(username, password, dob, address, telephone);
            conn.getServer().updateUsers();
            out.write("200\r\n");
            out.flush();
        } catch (SQLException e)
        {
            e.printStackTrace();
        }
    }

}
