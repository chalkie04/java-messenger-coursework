package msgServer;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;

public class GetAllMsgsCommand implements Command
{
    private BufferedReader in;
    private BufferedWriter out;
    private MsgSvrConnection conn;

    public GetAllMsgsCommand(BufferedReader in, BufferedWriter out, MsgSvrConnection serverConn)
    {
        this.in = in;
        this.out = out;
        this.conn = serverConn;
    }

    public void execute() throws IOException
    {
        /**
         * Completed by Luke on 14/03/2017.
         */

        String username = conn.getCurrentUser();

        Message[] msgs = null;

        msgs = conn.getServer().getMessages().getAllMessages(username);

        if (msgs != null)
        {
            out.write(MsgProtocol.MESSAGE + "\r\n" + msgs.length + "\r\n");
            for (Message i : msgs)
            {
                out.write(i.getSender() + "\r\n" + i.getDate() + "\r\n" + i.getContent() + "\r\n");
            }
            out.flush();
        } else
        {
            new ErrorCommand(in, out, conn, "No messages").execute();
        }

    }
}
