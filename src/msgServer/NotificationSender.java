package msgServer;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.Calendar;

/**
 * Created by Luke on 17/04/2017.
 */

//This class sends a message to the user if an event they have created is coming up, it is sent to their messages
//and can be viewed using the GetAllMessages command.
public class NotificationSender extends Thread{

    MessageServer serv;
    private Thread sender;
    private ResultSet notifications;
    private Timestamp currentDate;

    public NotificationSender(MessageServer serv){
        this.serv = serv;

        System.out.println("Creating Sender..." );
    }
    public void run() {
        while (true) {
            currentDate = new Timestamp(Calendar.getInstance().getTime().getTime());
            System.out.println("Running Sender... @ "+currentDate);
            try {
                notifications = serv.db.executeQuery("SELECT * FROM test.notification");
                while (notifications.next()) {
                    Timestamp date = notifications.getTimestamp("date");
                    if (currentDate.after(date)){
                        String[] recps = notifications.getString("recp").split(" ");
                        for (int i = 0; i < recps.length; i++) {
                            Message m = new Message(recps[i], notifications.getString("user"), notifications.getString("event") + ", @ " + notifications.getTimestamp("date").toString());
                            serv.getMessages().addMessage(m);
                            System.out.println("Notification: " + notifications.getString("event") + " sent to: " + recps[i]);
                        }
                        serv.db.execute("DELETE FROM test.notification Where user = '" + notifications.getString("user")+ "' and " + "event = '" + notifications.getString("event") + "'");
                        System.out.println("Deleted Notification...");
                    }
                }
                Thread.sleep(60000);
            } catch (SQLException e) {
                e.printStackTrace();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
    public void start(){
        System.out.println("Starting Notification Sender...");
        if (sender == null){
            sender = new Thread(this);
            sender.start();
        }
    }
}
