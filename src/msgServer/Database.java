package msgServer;

import com.mysql.jdbc.CommunicationsException;
import com.sun.org.apache.xerces.internal.xs.StringList;
import com.sun.xml.internal.fastinfoset.util.StringArray;

import java.io.FileWriter;
import java.sql.*;
import java.util.*;

// The newInstance() call is a work around for some
// broken Java implementations
public class Database {
    public Connection conn;
    private List<String> possibleDatabases = Arrays.asList(new String[]{"coursework.ckqsu3arbcap.eu-west-1.rds.amazonaws.com/test?user=root&password=asdfghjkl100", "35.187.123.106/test?user=liam&password=asdfghjkl100"});

    public Database() {

        try {
            Class.forName("com.mysql.jdbc.Driver").newInstance();
            for (String database : possibleDatabases)
            {
                String dbURL = "jdbc:mysql://" + database;
                try
                {
                    conn = DriverManager.getConnection(dbURL);
                } catch (Exception e)
                {
                    System.out.println("Failed Database Connection");
                }

                if (conn != null)
                {
                    break;
                }
            }
            if (conn == null) {
                System.out.print("No database connection possible, please check your internet connection & the db status");
                System.exit(5);
            }

        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }


    }

    public boolean execute(String sql) throws SQLException {
        Statement stmt = null;
        stmt = conn.createStatement();
        boolean res = stmt.execute(sql);
        return res;
    }

    public ResultSet executeQuery(String query) throws SQLException

    {
        Statement stmt = null;
        stmt = conn.createStatement();
        ResultSet rs = stmt.executeQuery(query);
        return rs;
    }

    public boolean registerUser(String name, String password, String dob, String address, String telephone) throws SQLException
    {
        String SQL = "INSERT INTO test.users (name,password,dob,address,telephone) VALUES (\"" + name + "\" , \"" + password + "\",\"" + dob + "\",\"" + address + "\",\"" + telephone + "\")";

        System.out.println(SQL);
        return execute(SQL);
    }
    public void updateReg(Map<String, String> map, MsgSvrConnection SerConn) {
        String[] fields = {"name", "password", "dob", "address", "telephone"};
        for (String field : fields) {
            String FieldData = map.get(field);
            if (FieldData == null) {
                continue;
            }
            String user = SerConn.getCurrentUser();
            String SQL ="UPDATE users SET "+ field + "="+ "\"" + map.get(field) + "\"" +  " WHERE " + "name=" +  "\"" + user + "\"";
            System.out.println(SQL);
            if (field == "name")
            {
                SerConn.setCurrentUser(map.get(field));
            }
            try {
                execute(SQL);
            } catch (SQLException e) {
                e.printStackTrace();
            }

        }
    }

    public String getPassword(String user) throws SQLException
    {

        String SQL = "SELECT Password FROM users where users.name = " + user;
        ResultSet rs = executeQuery(SQL);
        rs.first();
        return "";
    }

    public Properties getUsers() {
        try {
            Properties users = new Properties();
            String SQL = "SELECT * from test.users";
            ResultSet rs = executeQuery(SQL);


            while (rs.next()) {
                users.put(rs.getString("name"), rs.getString("password"));
            }
            return users;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    public boolean createNotification(String user, String event, String recp, String date) throws SQLException {
        String SQL = "INSERT INTO test.notification (user, event, recp, date) VALUES (\"" + user + "\" , \"" + event + "\" , \"" + recp + "\" , \"" + date + "\")";
        System.out.println(SQL);
        return execute(SQL);
    }

    public ResultSet getUserNotifications(String user) throws SQLException {
        String SQL = "SELECT * FROM test.notification WHERE notification.user = " + "'" + user + "'";
        System.out.println(SQL);
        ResultSet rs = executeQuery(SQL);
        ResultSetMetaData rsm = rs.getMetaData();
        int columnsNumber = rsm.getColumnCount();

        while (rs.next()) {
            for (int i = 1; i <= columnsNumber; i++) {
                if (i > 1) System.out.print(",  ");
                String columnValue = rs.getString(i);
                System.out.print(columnValue + " " + rsm.getColumnName(i));
            }
            System.out.println("");
        }

        return rs;
    }

    public void updateNot(Map<String, String> map, String eventName, String eventDate) {
        String[] fields = {"event", "date", "recp"};
        String updateData = "";
        for (String field : fields) {
            String FieldData = map.get(field);
            if (FieldData == null) {
                continue;
            }
            updateData += field + "=" + "\"" + map.get(field) + "\" ";
            updateData += ",";
        }

        updateData = updateData.substring(0, updateData.length()-2);
        String SQL = "UPDATE notification SET " + updateData +
                " WHERE " + "event=" + "\"" + eventName + "\" AND "
                + "date=\"" + eventDate + "\"";
        System.out.println(SQL);
        try {
            execute(SQL);
        } catch (SQLException e) {
            e.printStackTrace();
        }

    }
}