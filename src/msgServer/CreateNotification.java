package msgServer;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.GregorianCalendar;

/**
 * Created by Luke and Mitch on 16/03/2017.
 */

//This Class allows a user to create a notification/reminder.
    //EG:
    //Event name (Java meeting)
    // Recipients (liam mitch luke arron)
    //Date (2017-04-25 17:30:00)

public class CreateNotification implements Command
{

    BufferedReader in;
    BufferedWriter out;
    MsgSvrConnection conn;


    public CreateNotification(BufferedReader in, BufferedWriter out, MsgSvrConnection conn)
    {
        this.in = in;
        this.out = out;
        this.conn = conn;
    }

    public void execute() throws IOException
    {
        if (conn.getCurrentUser() == null)
        {
            new ErrorCommand(in, out, conn, "Not Currently Logged In").execute();
            return;
        }

        String user = conn.getCurrentUser();

        conn.getServer().sendDebugPromts(out, "Event name:");
        String eventName = in.readLine();

        conn.getServer().sendDebugPromts(out, "Recipients (format: name name name):");
        String recipients = in.readLine();
        for (String recp :
                recipients.split(" "))
        {
            if (!conn.getServer().isValidUser(recp))
            {
                new ErrorCommand(in, out, conn, "Invalid users").execute();
                return;
            }
        }

        conn.getServer().sendDebugPromts(out, "Date: format: YYYY-MM-DD hh:mm:ss :");
        String date = in.readLine();

        try
        {
            conn.getServer().db.createNotification(user, eventName, recipients, date);
            out.write("200\r\n");
            out.flush();
        } catch (SQLException e)
        {
            new ErrorCommand(in, out, conn, "Invalid inputs").execute();
        }


    }
}
