package msgServer;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by Aaron on 15/03/2017.
 */

//This Command allows the client to update their registration details within the server.
    //Eg:
    //108 (Command Number)
    //Update List (address,telephone)
    //1st elements update data
    //2nd elements update data
public class UpdateRegistration implements Command
{
    private BufferedReader in;
    private BufferedWriter out;
    private MsgSvrConnection conn;

    public UpdateRegistration(BufferedReader in, BufferedWriter out, MsgSvrConnection serverConn)
    {
        this.in = in;
        this.out = out;
        this.conn = serverConn;
    }

    @Override
    public void execute() throws IOException
    {
        if (conn.getCurrentUser() == null)
        {
            new ErrorCommand(in, out, conn, "Not Currently Logged In").execute();
            return;
        }
        Map<String, String> map = new HashMap<>();
        conn.getServer().sendDebugPromts(out, "Update list (name,password,dob,address,telephone):");
        String[] UpdateList = in.readLine().split(",");
        for (String update : UpdateList)
        {
            String[] possibleFields = {"name", "password", "dob", "address", "telephone"};
            int count = 0;
            for (String possibleField : possibleFields)
            {
                if (update.equals(possibleField))
                {
                    count++;
                }
            }
            if (count == 0)
            {
                new ErrorCommand(in, out, conn, "Bad input fields").execute();
                return;

            }
        }

        for (String update : UpdateList)
        {

            conn.getServer().sendDebugPromts(out, "NEW " + update + ":");

            String data = in.readLine();
            if (update.equals("dob"))
            {
                try
                {
                    SimpleDateFormat sqlFormatter = new SimpleDateFormat("yyyy-MM-dd");
                    SimpleDateFormat inputFormatter = new SimpleDateFormat("dd/MM/yyyy");
                    Date date = inputFormatter.parse(data);
                    data = sqlFormatter.format(date);
                } catch (Exception e)
                {
                    new ErrorCommand(in, out, conn, "Date formatted wrong").execute();
                    return;
                }
            }
            map.put(update, data);
        }
        conn.getServer().db.updateReg(map, conn);
        conn.getServer().updateUsers();
        out.write("200\r\n");
        out.flush();


    }

}
