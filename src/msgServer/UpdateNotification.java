package msgServer;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by mitch on 18/04/2017.
 */

//This class allows the client to update a notification they have created.
    //EG:
    //111 (Command Number)
    //Enter Event Name (Tom)
    //Enter Event Date and Time (1994-05-16)
    //Update List (recipients,date)
    //Enter Update Data(luke, 1995-03-05)
public class UpdateNotification implements Command
{

    BufferedReader in;
    BufferedWriter out;
    MsgSvrConnection conn;

    public UpdateNotification(BufferedReader in, BufferedWriter out, MsgSvrConnection conn)
    {
        this.in = in;
        this.out = out;
        this.conn = conn;
    }

    @Override
    public void execute() throws IOException
    {
        if (conn.getCurrentUser() == null)
        {
            new ErrorCommand(in, out, conn, "Not Currently Logged In").execute();
            return;
        }

        try
        {


            Database database = conn.getServer().db;
            String user = conn.getCurrentUser();

            conn.getServer().sendDebugPromts(out, "Event name:");
            String eventName = in.readLine();

            conn.getServer().sendDebugPromts(out, "Event date (YYYY-MM-DD hh:mm:ss):");
            String eventTime = in.readLine();
            String query = "select event, date from notification where event=\"" + eventName + "\" AND date=\"" + eventTime + "\"";
            ResultSet res = null;
            res = database.executeQuery(query);

            if (res.next())
            {
                res.beforeFirst();
                Map<String, String> map = new HashMap<>();
                conn.getServer().sendDebugPromts(out, "Update list (event,date,recp):");
                String[] UpdateList = in.readLine().split(",");
                for (String update : UpdateList)
                {
                    String[] possibleFields = {"event", "date", "recp"};
                    int count = 0;
                    for (String possibleField : possibleFields)
                    {
                        if (update.equals(possibleField))
                        {
                            count++;
                        }
                    }
                    if (count == 0)
                    {
                        new ErrorCommand(in, out, conn, "Bad input fields").execute();
                        return;

                    }
                }

                for (String update : UpdateList)
                {
                    conn.getServer().sendDebugPromts(out, "NEW " + update + ":");
                    String data = in.readLine();
                    if (update.equals("recp") ) {
                        for (String recp :
                                data.split(" "))
                        {
                            if (!conn.getServer().isValidUser(recp))
                            {
                                new ErrorCommand(in, out, conn, "Invalid users").execute();
                                return;
                            }
                        }
                    }
                    map.put(update, data);
                }
                database.updateNot(map, eventName, eventTime);
                out.write("200\r\n");
                out.flush();
            } else
            {
                new ErrorCommand(in, out, conn, "No Notification found").execute();
            }
        }
        catch (SQLException e)
        {
            e.printStackTrace();
        }
    }
}
