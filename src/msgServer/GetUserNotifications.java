package msgServer;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.sql.SQLException;

/**
 * Created by Luke on 11/04/2017.
 */
public class GetUserNotifications implements Command{
    BufferedReader in;
    BufferedWriter out;
    MsgSvrConnection conn;

    public GetUserNotifications (BufferedReader in, BufferedWriter out, MsgSvrConnection conn){
        this.in = in;
        this.out = out;
        this.conn = conn;
    }
    public void execute() throws IOException {
        String user = conn.getCurrentUser();

        try {
            conn.getServer().db.getUserNotifications(user);
            out.write("200\r\n");
            out.flush();
        }
        catch (SQLException e){
            e.printStackTrace();
        }


    }
}

