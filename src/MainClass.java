/**
 * Created by Liam on 14/03/2017.
 */

import msgServer.MessageServer;

import java.io.IOException;

public class MainClass
{

    public static void main(String[] args) throws IOException
    {

        // create a new MessageServer using this port number
        MessageServer server = new MessageServer(6000);
        // set verbose to true if you want information
        // set it to false to turn off all messages
        server.setVerbose(true);
        // Now start the messaging service
        server.startService();
    }
}
